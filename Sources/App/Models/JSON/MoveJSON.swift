import Vapor
import HTTP
import Leaf

final class MoveJSON: Content {
    /// Identifier of the game.
    let gameID: UUID
    
    /// Position concerned by the chosen move.
    let position: Int
    
    /// Initializes all properties of the 'MoveJSON'.
    public init(_ gameID: UUID, _ position: Int) {
        self.gameID = gameID
        self.position = position
    }
}
