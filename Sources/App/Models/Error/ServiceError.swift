import Vapor
import Fluent

enum ServiceError: String, Error {
    case notFound = "service-not-found"
    case badRequest = "service-bad-request"
    case failedRemoving = "failed-removing-service"
    case missingResponse = "missing-response"
}

extension ServiceError: ErrorHTTPBodyRepresentable {
    static var allCases: [ServiceError] {
        return [.notFound, .badRequest, .failedRemoving, .missingResponse]
    }
    
    var name: String {
        switch self {
        case .notFound:
            return "Service not found"
        case .badRequest:
            return "Bad request"
        case .failedRemoving:
            return "Failed removing"
        case .missingResponse:
            return "Missing response"
        }
    }
    
    var message: String {
        switch self {
        case .notFound:
            return "The service you selected does not exist. Please choose a valid one."
        case .badRequest:
            return "The service you entered is incorrect. Please verify that you correctly entered all the necessary data."
        case .failedRemoving:
            return "The service failed to be removed."
        case .missingResponse:
            return "The response is missing."
        }
    }
    
    var httpResponseStatus: HTTPResponseStatus {
        switch self {
        case .notFound:
            return HTTPResponseStatus.notFound
        case .badRequest, .missingResponse:
            return HTTPResponseStatus.badRequest
        case .failedRemoving:
            return HTTPResponseStatus.internalServerError
        }
    }
}

/// Allows 'ServiceError' to be encoded to and decoded from HTTP messages.
extension ServiceError: Content {
    /// Encode the 'ErrorHTTPBodyRepresentable' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorHTTPBodyRepresentable' from JSON to Object
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = ServiceError.allCases.filter { $0.code == code.code }.first!
    }
}

/// Allows 'ServiceError' to be used as a dynamic parameter in route definitions.
extension ServiceError: Parameter {
    typealias ResolvedParameter = ServiceError?
    
    static func resolveParameter(_ parameter: String, on container: Container) throws -> ServiceError? {
        return ServiceError(rawValue: parameter)
    }
}
