import Vapor
import Fluent

enum BoardError: String, Error {
    case tooSmallWidth
    case tooBigWidth
    case tooSmallHeight
    case tooBigHeight
    case intersectionAlreadyTaken
    case cannotPlayForNoLiberty
    case historyTooShort
    case pieceNotKillable
    case pieceNotGroupable
    case redundantMove
}

extension BoardError: ErrorHTTPBodyRepresentable {
    static var allCases: [BoardError] {
        return [.tooSmallWidth, .tooBigWidth, .tooSmallHeight, .tooBigHeight, .intersectionAlreadyTaken, .cannotPlayForNoLiberty,
                .historyTooShort, .pieceNotKillable, .pieceNotGroupable, .redundantMove]
    }
    
    var name: String {
        switch self {
        case .tooSmallWidth:
            return "Width too small"
        case .tooBigWidth:
            return "Width too big"
        case .tooSmallHeight:
            return "Height too small"
        case .tooBigHeight:
            return "Height too big"
        case .intersectionAlreadyTaken:
            return "Intersection already taken"
        case .cannotPlayForNoLiberty:
            return "Cannot play for no liberty"
        case .historyTooShort:
            return "History too short"
        case .pieceNotKillable:
            return "Piece not killable"
        case .pieceNotGroupable:
            return "Piece not groupable"
        case .redundantMove:
            return "Redundant move"
        }
    }
    
    var message: String {
        switch self {
        case .tooSmallWidth:
            return "The width position you entered is too small."
        case .tooBigWidth:
            return "The width position you entered is too far from the board."
        case .tooSmallHeight:
            return "The height position you entered is too small."
        case .tooBigHeight:
            return "The height position you entered is too far from the board."
        case .intersectionAlreadyTaken:
            return "The intersection you chose has already been taken."
        case .cannotPlayForNoLiberty:
            return "The play you did would lead to no liberty for your new piece."
        case .historyTooShort:
            return "The history is too short to do what you asked for."
        case .pieceNotKillable:
            return "The piece you kill must be either black or white."
        case .pieceNotGroupable:
            return "The piece you selected must be either black or white."
        case .redundantMove:
            return "The move you made is redundant."
        }
    }
    
    var httpResponseStatus: HTTPResponseStatus {
        switch self {
        case .tooSmallWidth, .tooBigWidth, .tooSmallHeight, .tooBigHeight, .intersectionAlreadyTaken,
             .cannotPlayForNoLiberty, .historyTooShort, .pieceNotKillable, .pieceNotGroupable, .redundantMove:
            return HTTPResponseStatus.badRequest
        }
    }
}

/// Allows 'BoardError' to be encoded to and decoded from HTTP messages.
extension BoardError: Content {
    /// Encode the 'ErrorHTTPBodyRepresentable' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorHTTPBodyRepresentable' from JSON to Object
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = BoardError.allCases.filter { $0.code == code.code }.first!
    }
}
