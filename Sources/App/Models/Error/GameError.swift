import Vapor
import Fluent

enum GameError: String, Error {
    case youAreAlreadyPlaying
    case playerAlreadyPlaying
    case gameDoesNotExist
    case boardDoesNotExist
    case intersectionAlreadyTaken
    case notYourTurn
}

extension GameError: ErrorHTTPBodyRepresentable {
    static var allCases: [GameError] {
        return [.youAreAlreadyPlaying, .playerAlreadyPlaying, .gameDoesNotExist, .boardDoesNotExist, .intersectionAlreadyTaken,
                .notYourTurn]
    }
    
    var name: String {
        switch self {
        case .youAreAlreadyPlaying:
            return "You are already playing"
        case .playerAlreadyPlaying:
            return "Player already playing"
        case .gameDoesNotExist:
            return "Game does not exist"
        case .boardDoesNotExist:
            return "Board does not exist"
        case .intersectionAlreadyTaken:
            return "Intersection already taken"
        case .notYourTurn:
            return "Not your turn"
        }
    }
    
    var message: String {
        switch self {
        case .youAreAlreadyPlaying:
            return "You have an unfinished game that does not allow you to defy someone else."
        case .playerAlreadyPlaying:
            return "The player you selected is already playing."
        case .gameDoesNotExist:
            return "The game you selected does not exist."
        case .boardDoesNotExist:
            return "The board you selected does not exist or does not belong to the game you are playing."
        case .intersectionAlreadyTaken:
            return "The intersection you chose to put your piece on is already taken."
        case .notYourTurn:
            return "It is not your turn to play !"
        }
    }
    
    var httpResponseStatus: HTTPResponseStatus {
        switch self {
        case .youAreAlreadyPlaying, .playerAlreadyPlaying, .gameDoesNotExist, .boardDoesNotExist, .intersectionAlreadyTaken,
             .notYourTurn:
            return HTTPResponseStatus.badRequest
        }
    }
}

/// Allows 'GameError' to be encoded to and decoded from HTTP messages.
extension GameError: Content {
    /// Encode the 'ErrorHTTPBodyRepresentable' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorHTTPBodyRepresentable' from JSON to Object
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = GameError.allCases.filter { $0.code == code.code }.first!
    }
}
