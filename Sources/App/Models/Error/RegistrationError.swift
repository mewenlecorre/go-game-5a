import Vapor
import Fluent

enum RegistrationError: String, Error {
    case usernameNotAvailable = "username-not-available"
    case mailNotAvailable = "mail-not-available"
    case passwordsNotMatching = "password-not-matching"
}

extension RegistrationError: ErrorHTTPBodyRepresentable {
    static var allCases: [RegistrationError] {
        return [.usernameNotAvailable, .mailNotAvailable, .passwordsNotMatching]
    }
    
    var name: String {
        switch self {
        case .usernameNotAvailable:
            return "Unavailable username"
        case .mailNotAvailable:
            return "Unavailable mail"
        case .passwordsNotMatching:
            return "Passwords not matching"
        }
    }
    
    var message: String {
        switch self {
        case .usernameNotAvailable:
            return "The username you chose is unavailable."
        case .mailNotAvailable:
            return "The mail you chose has already been taken."
        case .passwordsNotMatching:
            return "The passwords you entered do not match."
        }
    }
    
    var httpResponseStatus: HTTPResponseStatus {
        switch self {
        case .usernameNotAvailable, .mailNotAvailable, .passwordsNotMatching:
            return HTTPResponseStatus.badRequest
        }
    }
}

/// Allows 'RegistrationError' to be encoded to and decoded from HTTP messages.
extension RegistrationError: Content {
    /// Encode the 'ErrorHTTPBodyRepresentable' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorHTTPBodyRepresentable' from JSON to Object
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = RegistrationError.allCases.filter { $0.code == code.code }.first!
    }
}
