import Vapor
import Fluent

enum LeafCustomTagError: String, Error {
    case missingParameter = "missing-parameter"
    case tooManyParameters = "too-many-parameters"
}

extension LeafCustomTagError: ErrorHTTPBodyRepresentable {
    static var allCases: [LeafCustomTagError] {
        return [.missingParameter, .tooManyParameters]
    }
    
    var name: String {
        switch self {
        case .missingParameter:
            return "Missing parameter"
        case .tooManyParameters:
            return "Too many parameters"
        }
    }
    
    var message: String {
        switch self {
        case .missingParameter:
            return "There is a parameter missing to your custom tag."
        case .tooManyParameters:
            return "You entered too many parameters to your custom tag."
        }
    }
    
    var httpResponseStatus: HTTPResponseStatus {
        switch self {
        case .missingParameter, .tooManyParameters:
            return HTTPResponseStatus.badRequest
        }
    }
}

/// Allows 'LeafCustomTagError' to be encoded to and decoded from HTTP messages.
extension LeafCustomTagError: Content {
    /// Encode the 'ErrorHTTPBodyRepresentable' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorHTTPBodyRepresentable' from JSON to Object
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = LeafCustomTagError.allCases.filter { $0.code == code.code }.first!
    }
}

/// Allows 'LeafCustomTagError' to be used as a dynamic parameter in route definitions.
extension LeafCustomTagError: Parameter {
    typealias ResolvedParameter = LeafCustomTagError?
    
    static func resolveParameter(_ parameter: String, on container: Container) -> LeafCustomTagError? {
        return LeafCustomTagError(rawValue: parameter)
    }
}
