import Vapor
import FluentMySQL
import Authentication

final class User: MySQLUUIDModel {
    /// The unique identifier for this user.
    var id: UUID?
    
    /// Username used by the user throughout the application.
    var username: String
    
    /// Mail allowing to contact or to retrieve lost identifiers.
    var mail: String
    
    /// Password used to control the user's identity.
    var password: String
    
    /// Number of victories and defeats.
    var victories = 0
    var defeats = 0
    
    /// Ratio of victories that the player has.
    var victoryRatio: Float {
        guard victories + defeats > 0 else {
            return 0
        }
        
        return Float(victories / (victories + defeats))
    }
    
    /// Initializer of the basic properties of the 'User'.
    init(username: String, mail: String, password: String) {
        self.username = username
        self.mail = mail
        self.password = password
    }
    
    /// Initializer of all properties of the 'User'.
    init(_ id: UUID?, _ username: String, _ mail: String, _ password: String, _ victories: Int, _ defeats: Int) {
        self.id = id
        self.username = username
        self.mail = mail
        self.password = password
        self.victories = victories
        self.defeats = defeats
    }
    
    final class Public: Content {
        var id: UUID?
        var username: String
        var mail: String
        var victories: Int
        var defeats: Int
        var victoryRatio: Float
        
        init(_ id: UUID?, _ username: String, _ mail: String, _ victories: Int, _ defeats: Int, _ victoryRatio: Float) {
            self.id = id
            self.username = username
            self.mail = mail
            self.victories = victories
            self.defeats = defeats
            self.victoryRatio = victoryRatio
        }
    }
}

extension User {
    func toPublic() -> User.Public {
        return User.Public(id, username, mail, victories, defeats, victoryRatio)
    }
}

extension Future where T: User {
    func toPublic() -> Future<User.Public> {
        return map(to: User.Public.self) { (user) in
            return user.toPublic()
        }
    }
}

/// Allows 'User' to be used as a dynamic parameter in route definitions.
extension User: Parameter { }

/// Allows 'User' to be encoded to and decoded from HTTP messages.
extension User: Content { }

extension User: Migration { }

extension User: PasswordAuthenticatable {
    static var usernameKey: WritableKeyPath<User, String> {
        return \User.username
    }
    
    static var passwordKey: WritableKeyPath<User, String> {
        return \User.password
    }
}

extension User: SessionAuthenticatable { }

/// Enumeration responsible from keeping track of the properties
/// used to encode and decode an 'Application' instance.
private enum UserKeys: String, CodingKey {
    case id
    case username
    case mail
    case password
    case victories
    case defeats
    case victoryRatio
}

extension User {
    var game: Children<User, Game> {
        return children(\.blackUserID)
    }
    
    var discussion: Children<User, DiscussionMessage> {
        return children(\.authorID)
    }
}
