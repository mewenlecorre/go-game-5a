import Vapor
import FluentMySQL
import Authentication

final class RegistrationUser: Content {
    /// Username used by the user throughout the application.
    var username: String
    
    /// Mail allowing to contact or to retrieve lost identifiers.
    var mail: String
    
    /// Password used to control the user's identity.
    var password: String
    
    /// Second typed password to confirm there has been no mistake.
    var passwordConfirm: String
    
    func isPasswordConfirmed() -> Bool {
        return password == passwordConfirm;
    }
    
    init(username: String, mail: String, password: String, passwordConfirm: String) {
        self.username = username
        self.mail = mail
        self.password = password
        self.passwordConfirm = passwordConfirm
    }
}

final class LoginUser: Content {
    /// Username used by the user throughout the application.
    var username: String
    
    /// Password used to control the user's identity.
    var password: String

    
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}
