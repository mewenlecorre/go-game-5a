import Vapor

/// A type that makes errors readable.
///
/// It inherits from multiple types and allows an 'ErrorReadable' typed instance to
/// be translated from JSON to Object and vice versa and provides an interface that allows a type
/// to be more easily debugged in the case of an error.
///
/// Conforming to the 'ErrorReadable' Protocol
/// ============================================
///
/// Add 'ErrorReadable' conformance to your custom types by defining a 'name',
/// a 'message', a 'httpResponseStatus' and an 'allCases' property.
///
/// Since it also conforms to 'CaseIterable', this last property should return all cases
/// the enumeration has as following:
///
///     enum ApplicationError: ErrorReadable {
///         case notFound
///         case badRequest
///
///         static var allCases: [ApplicationError] {
///             return [.notFound, .badRequest]
///         }
///     }
protocol ErrorReadable: Content, Debuggable, CaseIterable {
    /// Name of the 'Error'.
    var name: String { get }
    
    /// Message displayed by the 'Error'.
    var message: String { get }
    
    /// HTTP status of the 'Error'.
    var httpResponseStatus: HTTPResponseStatus { get }
}

/// The 'code' of the error comes directly from the HTTPResponseStatus enum.
extension ErrorReadable {
    /// HTTP code of the 'Error'.
    var code: Int {
        return Int(self.httpResponseStatus.code)
    }
}

/// Allows 'Debuggable' errors to be more easily debugged.
extension ErrorReadable where Self: Debuggable {
    var identifier: String {
        return self.name
    }
    
    var reason: String {
        return self.message
    }
}

/// Allows 'ErrorReadable' to be encoded to and decoded from HTTP messages.
extension ErrorReadable where Self: Content, Self: CaseIterable {
    /// Encode the 'ErrorReadable' from an Object to JSON.
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ErrorReadableKeys.self)
        
        try container.encode(name, forKey: .name)
        try container.encode(message, forKey: .message)
        try container.encode(code, forKey: .code)
    }
    
    /// Decode the 'ErrorReadable' from JSON to an Object.
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ErrorReadableKeys.self)
        
        let code = try container.decode(HTTPCode.self, forKey: .code)
        
        self = Self.allCases.filter { $0.code == code.code }.first!
    }
}

public enum ErrorReadableKeys: String, CodingKey {
    case name
    case message
    case code
}
