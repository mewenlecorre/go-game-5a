import Vapor

/// A type with a customized textual representation.
///
/// It inherits from multiple types that allows an 'ErrorHTTPBodyRepresentable'
/// instance to be converted to a lossless HTTP body representation of type
/// 'String'.
///
/// Conforming to the 'ErrorHTTPBodyRepresentable' Protocol
/// =======================================================
///
/// Add 'ErrorHTTPBodyRepresentable' conformance to your custom types by
/// defining a 'name', a 'message', a 'httpResponseStatus' and an 'allCases' property.
///
/// Since it also conforms to 'CaseIterable', this last property should return all cases
/// the enumeration has as following:
///
///     enum ApplicationError: ErrorHTTPBodyRepresentable {
///         case notFound
///         case badRequest
///
///         static var allCases: [ApplicationError] {
///             return [.notFound, .badRequest]
///         }
///     }
///
/// The 'decription' and 'debugDescription' property will then both provide a textual
/// representation of any 'ErrorHTTPBodyRepresentable' instance in a JSON format allowed
/// by the use of the 'Content' protocol.
protocol ErrorHTTPBodyRepresentable: ErrorReadable, LosslessHTTPBodyRepresentable { }

/// Allows 'ErrorHTTPBodyRepresentable' to be both 'CustomStringConvertible' and 'CustomDebugStringConvertible'.
extension ErrorHTTPBodyRepresentable where Self: CustomStringConvertible, Self: CustomDebugStringConvertible {
    /// A textual representation of an 'ErrorHTTPBodyRepresentable' instance.
    ///
    /// It uses the encoder provided by the 'Encodable' protocol and the
    /// 'LosslessDataConvertible' in order to convert the return expression
    /// of type 'Data' into return type 'String'.
    ///
    /// Please note that the documentation discourages calling this property
    /// directly. They say that "instead, convert an instance of any type to a
    /// string by using the 'String(describing:)' initializer."
    var description: String {
        return try! JSONEncoder().encode(self).convert()
    }
    
    /// The 'description' and 'debugDecription' properties are returning the same value.
    var debugDescription: String {
        return self.description
    }
}

/// Allows 'ErrorHTTPBodyRepresentable' to be converted to 'HTTPBody'.
extension ErrorHTTPBodyRepresentable {
    func convertToHTTPBody() -> HTTPBody {
        return HTTPBody(string: String(describing: self))
    }
}

