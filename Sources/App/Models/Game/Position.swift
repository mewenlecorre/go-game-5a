import Vapor

struct Position: Content {
    let width: Int
    let height: Int
    let iterator: Int
    let inArray: PositionInArray
    
    init(width: Int, height: Int, iterator: Int? = nil) {
        self.width = width
        self.height = height
        self.iterator = iterator != nil ? iterator! : (height - 1) * Board.BoardSize.height + width
        self.inArray = PositionInArray(width, height)
    }
    
    init(iterator: Int) {
        let height = iterator != 0 ? Int(iterator / Board.BoardSize.width) : 0
        let width = iterator - (Board.BoardSize.height * height)

        self.init(width: width + 1, height: height + 1, iterator: iterator)
    }
}

struct PositionInArray: Content {
    let width: Int
    let height: Int
    
    init(_ width: Int, _ height: Int) {
        self.width = width - 1
        self.height = height - 1
    }
}
