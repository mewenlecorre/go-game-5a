import Vapor
import FluentMySQL

final class DiscussionMessage: MySQLUUIDModel {
    /// The unique identifier for this message.
    var id: UUID?
    
    /// Game linked to this message.
    let gameID: UUID
    
    let authorID: UUID
    
    let content: String
    
    /// Date at which the message has been sent.
    let date: Date
    
    init(forGame gameID: UUID, sentBy authorID: UUID, containing content: String, sentAt date: Date, withIDOf id: UUID? = nil) {
        self.id = id
        self.gameID = gameID
        self.authorID = authorID
        self.content = content
        self.date = date
    }
}

extension DiscussionMessage {
    var game: Parent<DiscussionMessage, Game> {
        return parent(\.gameID)
    }
    
    var author: Parent<DiscussionMessage, User> {
        return parent(\.authorID)
    }
}

/// Allows 'DiscussionMessage' to be used as a dynamic parameter in route definitions.
extension DiscussionMessage: Parameter { }

/// Allows 'DiscussionMessage' to be encoded to and decoded from HTTP messages.
extension DiscussionMessage: Content { }

extension DiscussionMessage: Migration { }
