import Vapor
import FluentMySQL
import Authentication

final class Game: MySQLUUIDModel {
    /// The unique identifier for this game.
    var id: UUID?
    
    /// Player using the black pieces
    let blackUserID: UUID
    
    /// Player using the white pieces
    let whiteUserID: UUID
    
    /// Indicates if the game is still open or not.
    var isOpen: Bool
    
    /// Initializer of the basic properties of the 'Game'.
    init(blackUserID: UUID, whiteUserID: UUID, isOpen: Bool) {
        self.blackUserID = blackUserID
        self.whiteUserID = whiteUserID
        self.isOpen = isOpen
    }
    
    /// Initializer of all properties of the 'Game'.
    init(_ id: UUID?, _ blackUserID: UUID, _ whiteUserID: UUID, _ isOpen: Bool) {
        self.id = id
        self.blackUserID = blackUserID
        self.whiteUserID = whiteUserID
        self.isOpen = isOpen
    }
}

extension Game {
    var blackUser: Parent<Game, User> {
        return parent(\.blackUserID)
    }
    
    var whiteUser: Parent<Game, User> {
        return parent(\.whiteUserID)
    }
    
    var board: Children<Game, Board> {
        return children(\.gameID)
    }
    
    var discussion: Children<Game, DiscussionMessage> {
        return children(\.gameID)
    }
}

/// Allows 'Game' to be used as a dynamic parameter in route definitions.
extension Game: Parameter { }

/// Allows 'Game' to be encoded to and decoded from HTTP messages.
extension Game: Content { }

extension Game: Migration { }
