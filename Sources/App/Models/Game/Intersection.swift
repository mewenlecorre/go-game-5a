import Vapor
import FluentMySQL

final class Intersection: MySQLUUIDModel {
    /// The unique identifier for this board state.
    var id: UUID?
    
    /// Board concerned by the current description.
    var boardID: UUID?
    var boardStateID: UUID?
    
    let position: Position
    
    var width: Int {
        return position.width
    }
    
    var height: Int {
        return position.height
    }
    
    var state: State
    
    enum State: String, Content, ReflectionDecodable, MySQLEnumType, TemplateDataRepresentable {
        func convertToTemplateData() throws -> TemplateData {
            return .string(self.rawValue)
        }
        
        static func reflectDecoded() throws -> (State, State) {
            return (.black, white)
        }
        
        case black
        case white
    }
    
    init(at position: Position, for state: State, on boardID: UUID? = nil, orBoardState boardStateID: UUID? = nil, as id: UUID? = nil) {
        self.id = id
        self.boardID = boardID
        self.state = state
        self.position = position
        self.boardStateID = boardStateID
    }
}

extension Intersection {
    var board: Parent<Intersection, Board> {
        return parent(\.boardID!)
    }
    
    var boardState: Parent<Intersection, BoardState> {
        return parent(\.boardStateID!)
    }
}

/// Allows 'Intersection' to be used as a dynamic parameter in route definitions.
extension Intersection: Parameter { }

/// Allows 'Intersection' to be encoded to and decoded from HTTP messages.
extension Intersection: Content {
    /// Encode the 'Intersection' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(boardID, forKey: .boardID)
        try container.encode(boardStateID, forKey: .boardStateID)
        try container.encode(width, forKey: .width)
        try container.encode(height, forKey: .height)
        try container.encode(state, forKey: .state)
    }
    
    /// Decode the 'Intersection' from JSON to Object
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let id = try container.decodeIfPresent(UUID.self, forKey: .id)
        let boardID = try container.decodeIfPresent(UUID.self, forKey: .boardID)
        let boardStateID = try container.decodeIfPresent(UUID.self, forKey: .boardStateID)
        
        let width = try container.decode(Int.self, forKey: .width)
        let height = try container.decode(Int.self, forKey: .height)
        let position = Position(width: width, height: height)
        
        let state = try container.decode(State.self, forKey: .state)
        
        self.init(at: position, for: state, on: boardID, orBoardState: boardStateID, as: id)
    }
}

extension Intersection: Migration {
    static func prepare(on conn: MySQLConnection) -> Future<Void> {
        return MySQLDatabase.create(Intersection.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.boardID)
            builder.field(for: \.boardStateID)
            builder.field(for: \.width)
            builder.field(for: \.height)
            builder.field(for: \.state)
        }
    }
}

extension Intersection: Equatable {
    static func == (lhs: Intersection, rhs: Intersection) -> Bool {
        return lhs.id == rhs.id && lhs.boardID == rhs.boardID &&
            lhs.position.width == rhs.position.width && lhs.position.height == rhs.position.height && lhs.state == rhs.state
    }
}

private enum CodingKeys: String, CodingKey {
    case id
    case boardID
    case boardStateID
    case width
    case height
    case state
}
