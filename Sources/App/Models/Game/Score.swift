import Vapor
import FluentMySQL

final class Score: MySQLUUIDModel {
    /// The unique identifier for this score.
    var id: UUID?
    
    /// Board concerned by the current description.
    var boardID: UUID?
    var boardStateID: UUID?
    
    var black = 0
    var white = 0
    
    init(forBoard boardID: UUID?) {
        self.boardID = boardID
    }
    
    init(forBoardState boardStateID: UUID?) {
        self.boardStateID = boardStateID
    }
}


extension Score {
    var board: Parent<Score, Board> {
        return parent(\.boardID!)
    }
    
    var boardState: Parent<Score, BoardState> {
        return parent(\.boardStateID!)
    }
}

/// Allows 'Score' to be used as a dynamic parameter in route definitions.
extension Score: Parameter { }

/// Allows 'Score' to be encoded to and decoded from HTTP messages.
extension Score: Content { }

extension Score: Migration { }

extension Score: Equatable {
    static func == (lhs: Score, rhs: Score) -> Bool {
        return lhs.black == rhs.black && lhs.white == rhs.white
    }
}
