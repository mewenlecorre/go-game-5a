import Vapor
import FluentMySQL

final class Board: MySQLUUIDModel {
    /// The unique identifier for this board.
    var id: UUID?
    
    var gameID: UUID
    
    /// Used to manage the game with the MySQLUUIDModel properties that have been queried
    struct JoinedProperties: Content {
        /// Score of the game
        var score: Score
        
        /// Board array containing the state (black or white) of every taken intersection on the board.
        var intersections: [Intersection]?
        
        /// Game history and history allowing to perform a 'redo' action
        var history = [BoardState]()
        
        init(score: Score, intersections: [Intersection]? = nil, history: [BoardState]? = nil) {
            self.score = score
            self.intersections = intersections
            self.history = history != nil ? history! : [BoardState]()
        }
    }
    
    var joinedProperties: JoinedProperties
    
    /// Contains the identity of the player that has to play.
    var turn: Turn
    
    var nextTurn: Turn {
        switch self.turn {
        case .black:
            return .white
        case .white:
            return .black
        }
    }
    
    struct BoardSize {
        static let width = 19
        static let height = 19
    }
    
    enum Turn: String, Content, ReflectionDecodable, MySQLEnumType {
        static func reflectDecoded() throws -> (Board.Turn, Board.Turn) {
            return (.black, .white)
        }
        
        case black
        case white
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case gameID
        case turn
    }

    init(forGame gameID: UUID) {
        self.gameID = gameID
        self.turn = .black
        self.joinedProperties = JoinedProperties(score: Score(forBoard: nil), intersections: [Intersection](), history: [BoardState]())
    }
    
    init(_ id: UUID?, forGame gameID: UUID, atTurn turn: Board.Turn = .black) {
        self.id = id 
        self.gameID = gameID
        self.turn = turn
        self.joinedProperties = JoinedProperties(score: Score(forBoard: id), intersections: [Intersection](), history: [BoardState]())
    }
    
    func checkIndex(iterator: Int) throws {
        let position = Position(iterator: iterator)
        try checkIndex(position: position)
    }
    
    func checkIndex(position: Position) throws {
        if position.width < 1 {
            throw BoardError.tooSmallWidth
        }
        
        if position.width > BoardSize.width {
            throw BoardError.tooBigWidth
        }
        
        if position.height < 1 {
            throw BoardError.tooSmallHeight
        }
        
        if position.height > BoardSize.height {
            throw BoardError.tooBigHeight
        }
    }
    
    func getItem(iterator: Int) throws -> Intersection.State? {
        let position = Position(iterator: iterator)
        try checkIndex(position: position)
        
        return try getItem(position: position)
    }
    
    func getItem(position: Position) throws -> Intersection.State? {
        try checkIndex(position: position)
        
        if let intersections = self.joinedProperties.intersections, intersections.count > 0 {
            for intersection in intersections {
                if intersection.position.width == position.width && intersection.position.height == position.height {
                    return intersection.state
                }
            }

            return nil
        } else {
            return nil
        }

    }
    
    func setItem(iterator: Int, intersectionState: Intersection.State?) throws {
        let position = Position(iterator: iterator)
        try checkIndex(position: position)

        try self.setItem(position: position, intersectionState: intersectionState)
    }
    
    func setItem(position: Position, intersectionState: Intersection.State?) throws {
        try checkIndex(position: position)
        
        if self.joinedProperties.intersections == nil {
            self.joinedProperties.intersections = [Intersection]()
        }
        
        var iterator: Int?
        
        if self.joinedProperties.intersections != nil && self.joinedProperties.intersections!.count > 0 {
            for i in 1...self.joinedProperties.intersections!.count {
                if self.joinedProperties.intersections![i - 1].position.width == position.width &&
                    self.joinedProperties.intersections![i - 1].position.height == position.height {
                    iterator = i - 1
                }
            }
        }
        
        if iterator != nil {
            if intersectionState != nil {
                self.joinedProperties.intersections![iterator!].state = intersectionState!
            } else {
                self.joinedProperties.intersections!.remove(at: iterator!)
            }
        } else {
            if intersectionState != nil {
                self.joinedProperties.intersections! += [Intersection(at: position, for: intersectionState!, on: self.id!)]
            }
        }
    }

    /// Makes the move
    func move(position: Position) throws {
        /// Check that a player has not already taken the position
        if try getItem(position: position) != nil {
            throw BoardError.intersectionAlreadyTaken
        }

        /// Sets the item once an history entry has been added about the current state of the game
        pushHistory()
        try setItem(position: position, intersectionState: Intersection.State(rawValue: self.turn.rawValue)!)

        /// Check if pieces have been taken and need to be removed
        let taken = try takePieces(position: position)
        
        /// Check that the coordinates of the piece will have liberties once played.
        if taken == 0 {
            try checkForSuicide(position: position)
        }

        /// Check that the board is not in the same state as one of a player's last move.
        try checkForKo()

        flipTurn()
    }

    /// Checks if move is suicidal
    func checkForSuicide(position: Position) throws {
        if countLiberties(position: position) == 0 {
            _ = try popHistory()
            throw BoardError.cannotPlayForNoLiberty
        }
    }
    
    /// Checks if board state is redundant.
    func checkForKo() throws {
        let historySize = self.joinedProperties.history.count
        
        guard historySize > 1 else {
            return
        }
        
        guard self.joinedProperties.intersections == self.joinedProperties.history[historySize - 2].joinedProperties.intersections else {
            _ = try popHistory()
            throw BoardError.redundantMove
        }
    }
    
    /// Checks if any pieces were taken by the last move at the specified
    /// coordinates. If so, removes them from play and tallies resulting
    /// points.
    func takePieces(position: Position) throws -> Int {
        var scores = [Int]()
        let surroundingCoordinates = getSurroundingCoordinates(position: position).filter { position in
            return getNone(position: position) != nil
        }
        
        for surrounding in surroundingCoordinates {
            let liberties = getSurroundingCoordinates(position: surrounding).filter { position in
                return getNone(position: position) == nil
            }
            
            if getNone(position: surrounding) == Intersection.State(rawValue: self.nextTurn.rawValue)
                && liberties.count == 0 {
                self.joinedProperties.intersections = self.joinedProperties.intersections!.filter { intersection in
                    return intersection.position.iterator != surrounding.iterator
                }
                
                scores.append(1)
                tally(score: 1)
            }
        }
        
        return scores.reduce(0, +)
    }

    /// Iterates the turn counter.
    func flipTurn() {
        self.turn = self.nextTurn
    }

    /// Loads the specified game state.
    func loadState(boardState: BoardState) {
        self.joinedProperties.intersections = boardState.joinedProperties.intersections
        self.turn = boardState.turn
        self.joinedProperties.score = boardState.joinedProperties.score
    }

    /// Pushes game state onto history.
    func pushHistory() {
        self.joinedProperties.history.append(BoardState(nil, forBoard: self.id!, atTurn: self.turn, on: Date(), self.joinedProperties.score, self.joinedProperties.intersections))
    }

    /// Pops and loads game state from history.
    func popHistory() throws -> BoardState {
        guard self.joinedProperties.history.count > 0 else {
            throw BoardError.historyTooShort
        }
        
        let currentBoardState = BoardState(atTurn: self.turn, forBoard: self.id!)
        loadState(boardState: self.joinedProperties.history.removeLast())
        return currentBoardState
    }

    /// Adds points to the current turn's score.
    func tally(score: Int) {
        if self.turn == .black {
            self.joinedProperties.score.black += score
        } else {
            self.joinedProperties.score.white += score
        }
    }

    /// Same thing as Array.__getitem__, but returns None if coordinates are
    /// not within array dimensions.
    func getNone(position: Position) -> Intersection.State? {
        do {
            return try getItem(position: position)
        } catch {
            return nil
        }
    }

    /// Gets information about the surrounding locations for a specified
    /// coordinate. Returns a tuple of the locations clockwise starting from
    /// the top.
    func getSurroundingCoordinates(position: Position) -> [Position] {
        var coordinates = [Position(width: position.width, height: position.height - 1),
                        Position(width: position.width + 1, height: position.height),
                        Position(width: position.width, height: position.height + 1),
                        Position(width: position.width - 1, height: position.height)]
        
        coordinates = coordinates.filter { unique in
            return unique.width > 0 && unique.width <= Board.BoardSize.width && unique.height > 0 && unique.height <= Board.BoardSize.height
        }
        
        return coordinates
    }

    /// Recursively traverses adjacent locations of the same color to find all
    /// locations which are members of the same group.
    private func _getGroup(position: Position, currentCoordinates: [Position]) -> [Position] {
        let intersectionState = getNone(position: position)
        var newCoordinates = currentCoordinates
        
        /// Adds the coordinates of the intersection to the new coordinates
        newCoordinates.append(position)

        /// Get surrounding locations which have the same color and whose
        /// coordinates have not already been traversed
        let positions: [Position] = getSurroundingCoordinates(position: position).filter { position in
            return getNone(position: position) != nil
        }.filter { surroundingPosition in
            return try! getItem(position: surroundingPosition) == intersectionState
        }
        
        if positions.count == 0 {
            return Array(NSOrderedSet(array: newCoordinates)) as! [Position]
        } else {
            return Array(NSOrderedSet(array: positions.map {
                return _getGroup(position: $0, currentCoordinates: newCoordinates)
            }.flatMap { return $0 })) as! [Position]
        }
    }

    /// Gets the coordinates for all locations which are members of the same
    /// group as the location at the given coordinates.
    func getGroup(position: Position) throws -> [Position] {
        let intersection = try getItem(position: position)
        
        guard intersection != .black && intersection != .white else {
            throw BoardError.pieceNotGroupable
        }

        return self._getGroup(position: position, currentCoordinates: [Position]())
    }

    /// Kills a group of black or white pieces and returns its size for scoring.
    func killGroup(position: Position) throws -> Int {
        let intersection = try getItem(position: position)
        
        guard intersection != .black && intersection != .white else {
            throw BoardError.pieceNotKillable
        }

        let groupPositions = try getGroup(position: position)
        let score = groupPositions.count

        for intersectionPosition in groupPositions {
            try setItem(position: intersectionPosition, intersectionState: nil)
        }

        return score
    }

    /// Recursively traverses adjacent locations of the same color to find all
    /// surrounding liberties for the group at the given coordinates.
    private func _getLiberties(position: Position, currentCoordinates: [Position]) -> [Position] {
        let intersectionState = getNone(position: position)
        var newCoordinates = currentCoordinates

        /// Return coords of empty location (this counts as a liberty)
        if intersectionState == nil {
            return Array(NSOrderedSet(array: currentCoordinates + [position])) as! [Position]
        } else {
            /// Get surrounding locations which are empty or have the same color
            /// and whose coordinates have not already been traversed
            let positions: [Position] = getSurroundingCoordinates(position: position).filter { surroundingPosition in
                let surroundingState: Intersection.State?
                
                do {
                    surroundingState = try getItem(position: surroundingPosition)
                } catch {
                    surroundingState = nil
                }
                
                return surroundingState == intersectionState || surroundingState == nil
            }
        
            /// Marks the coordinates of the intersection to the new coordinates
            newCoordinates.append(position)
            
            /// Collect unique coordinates of surrounding liberties
            if positions.count == 0 {
                return [Position]()
            } else {
                return Array(NSOrderedSet(array: positions.map {
                    return _getLiberties(position: $0, currentCoordinates: newCoordinates)
                }.flatMap { return $0 })) as! [Position]
            }
        }
    }

    /// Gets the coordinates for liberties surrounding the group at the given coordinates.
    func getLiberties(position: Position) -> [Position]? {
        return _getLiberties(position: position, currentCoordinates: [Position]())
    }

    /// Gets the number of liberties surrounding the group at the given
    /// coordinates.
    func countLiberties(position: Position) -> Int {
        let liberties = self.getLiberties(position: position)
        return liberties != nil ? liberties!.count : 0
    }
    
    func countScore() -> [String: Int] {
        var countBlackIntersections = 0
        var countWhiteIntersections = 0
        
        if self.joinedProperties.intersections != nil {
            countBlackIntersections = self.joinedProperties.intersections!.filter { intersection in
                    return intersection.state == .black
            }.count
        
            countWhiteIntersections = self.joinedProperties.intersections!.filter { intersection in
                return intersection.state == .white
            }.count
        }
        
        return ["black": self.joinedProperties.score.black + countBlackIntersections, "white": self.joinedProperties.score.white + countWhiteIntersections]
    }
}

extension Board {
    var game: Parent<Board, Game> {
        return parent(\.gameID)
    }
    
    var score: Children<Board, Score> {
        return children(\.boardID)
    }
    
    var intersections: Children<Board, Intersection> {
        return children(\.boardID)
    }
    
    var history: Children<Board, BoardState> {
        return children(\.boardID)
    }
}

/// Allows 'Board' to be used as a dynamic parameter in route definitions.
extension Board: Parameter { }

/// Allows 'Board' to be encoded to and decoded from HTTP messages.
extension Board: Content {
    /// Encode the 'Board' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(gameID, forKey: .gameID)
        try container.encode(turn, forKey: .turn)
    }
    
    /// Decode the 'Board' from JSON to Object
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let id = try container.decodeIfPresent(UUID.self, forKey: .id)
        let gameID = try container.decode(UUID.self, forKey: .gameID)
        let turn = try container.decode(Turn.self, forKey: .turn)
        
        self.init(id, forGame: gameID, atTurn: turn)
    }
}

extension Board: Migration {
    static func prepare(on conn: MySQLConnection) -> Future<Void> {
        return MySQLDatabase.create(Board.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.gameID)
            builder.field(for: \.turn)
        }
    }
}
