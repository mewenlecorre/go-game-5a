import Vapor
import FluentMySQL

final class BoardState: MySQLUUIDModel {
    /// The unique identifier for this board state.
    var id: UUID?
    
    /// Board concerned by the current description.
    var boardID: UUID

    let turn: Board.Turn
    
    let date: Date
    
    var joinedProperties: JoinedProperties
    
    /// Used to manage the game with the MySQLUUIDModel properties that have been queried
    struct JoinedProperties: Content {
        /// Score of the game
        var score: Score
        
        /// Board array containing the state (black, white or empty) of every intersection on the board.
        var intersections: [Intersection]?
        
        init(score: Score, intersections: [Intersection]? = nil) {
            self.score = score
            self.intersections = intersections
        }
    }
    
    init(atTurn turn: Board.Turn, forBoard boardID: UUID, on date: Date = Date()) {
        self.boardID = boardID
        self.turn = turn
        self.date = date
        self.joinedProperties = JoinedProperties(score: Score(forBoardState: nil), intersections: nil)
    }
    
    init(_ id: UUID?, forBoard boardID: UUID, atTurn turn: Board.Turn = .black, on date: Date = Date()) {
        self.id = id
        self.boardID = boardID
        self.turn = turn
        self.date = date
        self.joinedProperties = JoinedProperties(score: Score(forBoardState: id), intersections: nil)
    }
    
    init(_ id: UUID?, forBoard boardID: UUID, atTurn turn: Board.Turn = .black, on date: Date = Date(), _ score: Score, _ intersections: [Intersection]?) {
        self.id = id
        self.boardID = boardID
        self.turn = turn
        self.date = date
        self.joinedProperties = JoinedProperties(score: score, intersections: intersections)
    }
}

extension BoardState {
    var board: Parent<BoardState, Board> {
        return parent(\.boardID)
    }
    
    var score: Children<BoardState, Score> {
        return children(\.boardStateID?)
    }
    
    var intersections: Children<BoardState, Intersection> {
        return children(\.boardStateID?)
    }
}

/// Allows 'BoardState' to be used as a dynamic parameter in route definitions.
extension BoardState: Parameter { }

/// Allows 'BoardState' to be encoded to and decoded from HTTP messages.
extension BoardState: Content {
    /// Encode the 'BoardState' from Object to JSON
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(boardID, forKey: .boardID)
        try container.encode(turn, forKey: .turn)
        try container.encode(date, forKey: .date)
    }
    
    /// Decode the 'BoardState' from JSON to Object
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let id = try container.decodeIfPresent(UUID.self, forKey: .id)
        let boardID = try container.decode(UUID.self, forKey: .boardID)
        let turn = try container.decode(Board.Turn.self, forKey: .turn)
        let date = try container.decode(Date.self, forKey: .date)
        
        self.init(id, forBoard: boardID, atTurn: turn, on: date)
    }
}

extension BoardState: Migration {
    static func prepare(on conn: MySQLConnection) -> Future<Void> {
        return MySQLDatabase.create(BoardState.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.boardID)
            builder.field(for: \.turn)
            builder.field(for: \.date)
        }
    }
}

extension BoardState: Equatable {
    static func == (lhs: BoardState, rhs: BoardState) -> Bool {
        let lhsIntersections = lhs.joinedProperties.intersections
        let rhsIntersections = rhs.joinedProperties.intersections
        
        return lhs.boardID == rhs.boardID &&
            lhs.joinedProperties.score == rhs.joinedProperties.score &&
            (lhsIntersections == rhsIntersections || ((lhsIntersections == nil || lhsIntersections == [Intersection]()) && (rhsIntersections == nil || rhsIntersections == [Intersection]())))
    }
}

private enum CodingKeys: String, CodingKey {
    case id
    case boardID
    case turn
    case date
}
