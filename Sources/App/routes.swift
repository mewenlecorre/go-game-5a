import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let authenticationController = AuthenticationController()
    try router.register(collection: authenticationController)
    
    let gameController = GameController()
    try router.register(collection: gameController)
}
