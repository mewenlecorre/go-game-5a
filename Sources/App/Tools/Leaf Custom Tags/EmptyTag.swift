import Vapor

final class EmptyTag: TagRenderer {
    init() { }
    
    func render(tag: TagContext) throws -> EventLoopFuture<TemplateData> {
        var isEmpty: Bool = false
        
        switch tag.parameters.count {
        case 0:
            throw LeafCustomTagError.missingParameter
        case 1:
            if let array = tag.parameters[0].array {
                isEmpty = array.isEmpty
            } else {
                isEmpty = true
            }
        default:
            throw LeafCustomTagError.tooManyParameters
        }
        
        return tag.container.future(TemplateData.bool(isEmpty))
    }
}
