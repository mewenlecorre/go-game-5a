import Vapor
import HTTP
import Leaf

final class HeaderView: Content {
    /**
     * Name of the web application.
     */
    let applicationName: String
    
    /**
     * Name of the page.
     */
    let pageName: String
    
    /**
     * Stylesheet used for the page.
     * The main folder /styles/ and the extension .css being the same for every stylesheet, it is not needed to put them.
     * Example: "home/index".
     */
    let style: String?
    
    /**
     * Initializes all properties of the 'HeaderView'.
     */
    public init(_ pageName: String, _ style: String?) {
        self.applicationName = "Go Game"
        self.pageName = pageName
        self.style = style
    }
}
