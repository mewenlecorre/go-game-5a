import Vapor
import HTTP
import Leaf

final class HomeView: Content {
    /// Header of the page.
    let header: HeaderView
    
    /// Initializes all properties of the 'HomeView'.
    public init(_ header: HeaderView) {
        self.header = header
    }
}
