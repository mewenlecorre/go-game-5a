import Vapor
import HTTP
import Leaf

final class GameView: Content {
    /// Header of the page.
    let header: HeaderView
    
    /// Information about the current user.
    let mySelf: User
    
    /// Information about the current game.
    let game: Game
    
    /// Information about the adversary being either the black or white piece.
    let adversary: User
    
    /// Board that will be printed on the page
    let board: [String]
    
    /// Color of the user playing
    let turnColor: String
    
    let myColor: String
    
    struct IntersectionShape: Content {
        let iterator: Int
        let state: Intersection
    }
    
    /// Initializes all properties of the 'GameView'.
    public init(_ header: HeaderView, _ mySelf: User, _ game: Game, _ adversary: User, _ board: Board, _ intersections: [Intersection?]) throws {
        self.header = header
        self.mySelf = mySelf
        self.game = game
        self.adversary = adversary
        self.turnColor = board.turn == .black ? "noirs" : "blancs"
        self.myColor = game.blackUserID == mySelf.id! ? "black" : "white"
        
        var newBoard = Array<String>(repeating: "empty", count: Board.BoardSize.width * Board.BoardSize.height)
        
        if intersections.count > 0 {
            for intersection in intersections {
                newBoard[intersection!.position.iterator - 1] = intersection!.state.rawValue
            }
        }
        
        self.board = newBoard
    }
}
