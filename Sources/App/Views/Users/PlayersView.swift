import Vapor
import HTTP
import Leaf

final class PlayersView: Content {
    /// Header of the page.
    let header: HeaderView
    
    /// User of the application.
    let mySelf: User.Public
    
    /// Players of the game
    let players: [User.Public]?
    
    /// Initializes all properties of the 'PlayersView'.
    public init(_ header: HeaderView, _ mySelf: User.Public, _ players: [User.Public]?) {
        self.header = header
        self.mySelf = mySelf
        self.players = players
    }
}
