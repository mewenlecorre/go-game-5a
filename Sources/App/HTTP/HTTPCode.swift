import Vapor
import Fluent

enum HTTPCode: Int {
    /**
     * 1xx — Information
     */
    case Continue = 100, SwitchingProtocols, Processing, EarlyHints
    
    /**
     * 2xx — Success
     */
    case OK = 200, Created, Accepted, NonAuthoritativeInformation, NoContent, ResetContent, PartialContent, MultiStatus, AlreadyReported, ContentDifferent = 210, IMUsed = 226
    
    /**
     * 3xx — Redirection
     */
    case MultipleChoices = 300, MovedPermanently, Found, SeeOther, NotModified, UseProxy, None, TemporaryRedirect, PermanentRedirect, TooManyRedirects = 310
    
    /**
     * 4xx — Web client error
     */
    case BadRequest = 400, Unauthorized, PaymentRequired, Forbidden, NotFound, MethodNotAllowed, NotAcceptable, ProxyAuthenticationRequired, RequestTimeOut, Conflict, Gone, LengthRequired, PreconditionFailed, RequestEntityTooLarge, RequestURITooLong, UnsupportedMediaType, RequestedRangeUnsatifiable, ExpectationFailed, ImATeapot, BadMapping = 421, UnprocessableEntity, Locked, MethodFailure, UnorderedCollection, UpgradeRequired, PreconditionRequired, TooManyRequests, RequestHeaderFieldsTooLarge = 431, NoResponse = 444, RetryWith = 449, BlockedByWindowsParentalControls, UnavailableForLegalReasons, UnrecoverableError = 456, SSLCertificateError = 495, SSLCertificateRequired, HTTPRequestSentToHTTPSPort, ClientClosedRequest = 499
    
    /**
     * 5xx — Server error
     */
    case InternalServerError = 500, NotImplemented, BadGateway, ServiceUnavailable, GatewayTimeOut, HTTPVersionNotSupported, VariantAlsoNegotiates, InsufficientStorage, LoopDetected, BandwithLimitExceeded, NotExtended, NetworkAuthenticationRequired, UnknownError = 520, WebServerIsDown, ConnectionTimedOut, OriginIsUnreachable, ATimeOutOccured, SSLHandshakeFailed, InvalidSSLCertificate, RailgunError
    
    var code: Int {
        return self.rawValue
    }
}

/// Allows 'HTTPCode' to be encoded to and decoded from HTTP messages.
extension HTTPCode: Content { }
