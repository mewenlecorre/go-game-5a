import Vapor
import FluentMySQL

final class GameManager {
    /// Checks if an user already has a game open.
    public static func isUserAvailableToPlay(whosIdendityIs user: User, on request: Request) -> Future<Bool> {
        return getOpenGame(for: user, on: request).map { return $0 == nil }
    }
    
    /// Retrieves the game a given player is currently playing.
    public static func getOpenGame(for user: User, on request: Request) -> Future<Game?> {
        return Game.query(on: request).group(.or) {
            $0.filter(\Game.blackUserID == user.id!).filter(\Game.whiteUserID == user.id!)
        }.filter(\Game.isOpen == true).first()
    }
    
    /// Retrieves the game a given player is currently playing.
    public static func getOpenGame(withIDOf id: UUID, on request: Request) -> Future<Game?> {
        return Game.query(on: request).filter(\Game.id == id).first()
    }
    
    /// Checks that the intersection has not been taken
    public static func isIntersectionAvailable(from board: Board, at position: Position, on request: Request) throws -> Future<Bool> {
        return try board.intersections.query(on: request).filter(\Intersection.width == position.width).filter(\Intersection.height == position.height).first().map { return $0 == nil }
    }
}
