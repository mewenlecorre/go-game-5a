import Vapor
import HTTP
import Leaf

final class LayoutController {
    /**
     * Builds the 'HeaderView' containing all variables needed for.
     *
     * @param ofPageNamed pageName: String Name of the page.
     * @param styledBy style: String? Path to the stylesheet.
     *
     * @return HeaderView All variables needed for the header.
     */
    static func header(ofPageNamed pageName: String, styledBy style: String?) -> HeaderView {
        return HeaderView(pageName, style)
    }
}
