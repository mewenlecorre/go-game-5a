import Vapor
import HTTP
import Leaf
import Authentication
import Crypto
import Fluent

final class AuthenticationController: RouteCollection {
    enum URLComponent: String {
        case authenticate
        case register
        case login
    }
    
    func boot(router: Router) throws {
        /// Create auth sessions middleware for user
        let session = User.authSessionsMiddleware()
        
        /// let basicAuthMiddleware = User.basicAuthMiddleware(using: BCryptDigest())
        
        /// Interrupts the request if the user is not authenticated
        let guardAuthMiddleware = User.guardAuthMiddleware()
        
        /// Redirects the visitor if he is not authenticated
        let redirectMiddleware = RedirectMiddleware<User>(path: "/" + URLComponent.authenticate.rawValue)
        
        let sessionRoute = router.grouped(session)
        
        /// Create a route group wrapped by these middlewares
        let auth = router.grouped(session, guardAuthMiddleware, redirectMiddleware)
        
        router.get(URLComponent.authenticate.rawValue, use: authenticate)
        router.post(URLComponent.register.rawValue, use: register)
        
        sessionRoute.post(URLComponent.login.rawValue, use: login)
        
        auth.get("logout", use: logout)
    }
    
    /// Page on which you can .
    func authenticate(_ request: Request) throws -> Future<View> {
        let path = "authentication/" + URLComponent.authenticate.rawValue
        
        let header = LayoutController.header(ofPageNamed: "", styledBy: path)
        let context = AuthenticateView(header)
        
        return try request.make(LeafRenderer.self).render(path, context)
    }
    
    /// Registers the user.
    func register(_ request: Request) throws -> Future<Response> {
        let newRequest = request.redirect(to: "/").makeRequest()
        
        do {
            return try request.content.decode(RegistrationUser.self).flatMap { registrationUser in
                guard registrationUser.isPasswordConfirmed() else {
                    throw RegistrationError.passwordsNotMatching
                }
                
                let user = User(username: registrationUser.username, mail: registrationUser.mail, password: registrationUser.password)
                
                return User.query(on: request).filter(\User.username == user.username).first().flatMap { matchingUsernames in
                    if let _ = matchingUsernames {
                        throw RegistrationError.usernameNotAvailable
                    }
                    
                    return User.query(on: request).filter(\User.mail == user.mail).first().flatMap { matchingMails in
                        if let _ = matchingMails {
                            throw RegistrationError.mailNotAvailable
                        }
                
                        user.password = try BCryptDigest().hash(user.password)
                        return user.save(on: request).map { _ in
                            return request.redirect(to: "/")
                        }
                    }
                }
            }
        } catch let error as ErrorHTTPBodyRepresentable {
            newRequest.http.body = error.convertToHTTPBody()
            return try request.redirect(to: "/" + URLComponent.authenticate.rawValue).encode(for: newRequest)
        }
    }
    
    /// Logs the user in.
    func login(_ request: Request) throws -> Future<Response> {
        let newRequest = request.redirect(to: "/").makeRequest()

        do {
            return try request.content.decode(LoginUser.self).flatMap { user in
                return User.authenticate(username: user.username, password: user.password, using: BCryptDigest(), on: request).map { user in
                    guard let user = user else {
                        throw RegistrationError.usernameNotAvailable ///////////////////////////////////////////// C'est pas la bonne erreur ///////////////////////////////////
                    }
                    
                    try request.authenticateSession(user)
                    return request.redirect(to: "/")
                }
            }
        } catch let error as ErrorHTTPBodyRepresentable {
            newRequest.http.body = error.convertToHTTPBody()
            return Future.map(on: newRequest) { return newRequest.redirect(to: "/" + URLComponent.authenticate.rawValue) }
        }
    }
    
    /// Logs the user out.
    func logout(_ request: Request) throws -> Future<Response> {
        try request.unauthenticateSession(User.self)
        return Future.map(on: request) { return request.redirect(to: "/" + URLComponent.authenticate.rawValue) }
    }
}
