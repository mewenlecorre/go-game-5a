import Vapor
import Fluent
import HTTP
import Leaf
import Authentication

final class GameController: RouteCollection {
    enum URLComponent: String {
        case home
        case game
        case players
        case play
        case move
        case pass
        
        static var directory: String {
            return "game"
        }
    }
    
    func boot(router: Router) throws {
        /// Create auth sessions middleware for user
        let session = User.authSessionsMiddleware()
        
        /// Redirects the visitor if he is not authenticated
        let redirectMiddleware = RedirectMiddleware<User>(path: "/" + AuthenticationController.URLComponent.authenticate.rawValue)
        
        /// Create a route group wrapped by this middleware
        let auth = router.grouped(session, redirectMiddleware)
        
        auth.get(use: home)
        auth.get(URLComponent.game.rawValue, use: game)
        auth.get(URLComponent.players.rawValue, use: players)
        auth.get(URLComponent.game.rawValue, URLComponent.play.rawValue, User.parameter, use: play)
        auth.get(URLComponent.game.rawValue, URLComponent.move.rawValue, Int.parameter, use: move)
        auth.get(URLComponent.game.rawValue, URLComponent.pass.rawValue, use: pass)
    }
    
    /// Home of the application.
    func home(_ request: Request) throws -> Future<View> {
        let mySelf = try request.authenticated(User.self)
        let path = URLComponent.directory + "/" + URLComponent.home.rawValue
        let header = LayoutController.header(ofPageNamed: "Accueil", styledBy: path)
        
        return GameManager.isUserAvailableToPlay(whosIdendityIs: mySelf!, on: request).flatMap { isUserAvailableToPlay in
            guard isUserAvailableToPlay else {
                return try self.game(request)
            }
            
            let context = HomeView(header)
            return try request.make(LeafRenderer.self).render(path, context)
        }
    }
    
    /// Board to play.
    func game(_ request: Request) throws -> Future<View> {
        let mySelf = try request.authenticated(User.self)
        let path = URLComponent.directory + "/" + URLComponent.game.rawValue
        let header = LayoutController.header(ofPageNamed: "Jouer", styledBy: path)
        
        return GameManager.isUserAvailableToPlay(whosIdendityIs: mySelf!, on: request).flatMap { isUserAvailableToPlay in
            guard !isUserAvailableToPlay else {
                return try self.home(request)
            }
            
            return GameManager.getOpenGame(for: mySelf!, on: request).flatMap { game in
                return try game!.board.query(on: request).first().flatMap { board in
                    let adversary = board!.turn == .black && game!.blackUserID == mySelf!.id! ? game!.whiteUser : game!.blackUser
                    return adversary.get(on: request).flatMap { adversary in
                        return try board!.intersections.query(on: request).all().flatMap { intersections in
                            let context = try GameView(header, mySelf!, game!, adversary, board!, intersections)
                            return try request.make(LeafRenderer.self).render(path, context)
                        }
                    }
                }
            }
        }
    }
    
    /// Puts a piece on the board if it is possible.
    func move(_ request: Request) throws -> Future<Response> {
        let mySelf = try request.authenticated(User.self)
        let newRequest = request.redirect(to: "/").makeRequest()
        
        do {
            /// Retrieves the data sent through a HTTP POST request
            let piecePosition = try request.parameters.next(Int.self)
            return GameManager.getOpenGame(for: mySelf!, on: request).flatMap { game in
                /// Checks that a game is open for the user
                guard game != nil else {
                    throw GameError.gameDoesNotExist
                }
                
                return try game!.board.query(on: request).first().flatMap { board -> EventLoopFuture<Board> in
                    guard board != nil else {
                        throw GameError.boardDoesNotExist
                    }
                    
                    /// Checks that it is the user's turn before allowing a move
                    guard (board!.turn == .black && game!.blackUserID == mySelf!.id!) ||
                        (board!.turn == .white && game!.whiteUserID == mySelf!.id!) else {
                            throw GameError.notYourTurn
                    }
                    
                    /// Checks that the given position exists
                    try board!.checkIndex(iterator: piecePosition)
                    let position = Position(iterator: piecePosition)
                    
                    /// Checks that the given position is available to play
                    return try board!.intersections.query(on: request).all().flatMap { intersections in
                        return try GameManager.isIntersectionAvailable(from: board!, at: position, on: request).flatMap { isIntersectionAvailable -> EventLoopFuture<Board> in
                            guard isIntersectionAvailable else {
                                throw GameError.intersectionAlreadyTaken
                            }
                            
                            board!.joinedProperties.intersections = intersections
                            
                            _ = intersections.map { intersection in
                                _ = intersection.delete(on: request)
                            }
                            
                            return try board!.score.query(on: request).first().flatMap { score in
                                /// Joins the history containing the state of the game had each turn,
                                /// joining the score and the board state to it.
                                return try board!.history.query(on: request).all().map { history -> [BoardState] in
                                    if history.count > 0 {
                                        return try history.compactMap { historyEntry in
                                            _ = try historyEntry.score.query(on: request).first().map { score in
                                                if score != nil {
                                                    historyEntry.joinedProperties.score = score!
                                                }
                                            }
                                            
                                            _ = try historyEntry.intersections.query(on: request).all().map { historyIntersections -> () in
                                                historyEntry.joinedProperties.intersections = historyIntersections
                                            }
                                            
                                            return historyEntry
                                        }
                                    }
                                    
                                    board!.joinedProperties.history = history
                                    return history
                                }
                            }.map { _ -> Board in
                                /// Plays the move with every side effect
                                try board!.move(position: position)
                                
                                /// Saves all Models joined to the board score of the game
                                _ = board!.joinedProperties.score.update(on: request)
                                /// Intersections on the board being an array of Intersection
                                _ = board!.joinedProperties.intersections!.compactMap { intersection in
                                    return intersection.create(on: request)
                                }

                                /// History of the game wich is an array of BoardState which has other Models joined to
                                _ = board!.joinedProperties.history.compactMap { historyEntry in
                                    historyEntry.boardID = board!.id!
                                    _ = historyEntry.save(on: request).map { savedHistoryEntry in
                                        historyEntry.joinedProperties.score.boardStateID = savedHistoryEntry.id!
                                        historyEntry.joinedProperties.score.boardID = nil
                                        historyEntry.joinedProperties.score.id = nil
                                        _ = historyEntry.joinedProperties.score.create(on: request)
                                        _ = historyEntry.joinedProperties.intersections.flatMap { intersections in
                                            _ = intersections.compactMap { intersection in
                                                intersection.boardStateID = savedHistoryEntry.id!
                                                intersection.boardID = nil
                                                intersection.id = nil
                                                _ = intersection.create(on: request)
                                                
                                            }
                                        }
                                    }
                                }
                                
                                return board!
                            }
                        }
                    }.transform(to: board!)
                }.flatMap { board -> Future<Board> in
                    /// Saves the board and redirects the user
                    return Board(board.id!, forGame: game!.id!, atTurn: board.turn).update(on: request)
                }.transform(to: request.redirect(to: "/"))
            }
        } catch let error as ErrorHTTPBodyRepresentable {
            newRequest.http.body = error.convertToHTTPBody()
            return try request.redirect(to: "/" + URLComponent.game.rawValue).encode(for: newRequest)
        }
    }
    
    /// List of players on the game.
    func players(_ request: Request) throws -> Future<View> {
        let mySelf = try request.authenticated(User.self)
        let path = URLComponent.directory + "/" + URLComponent.players.rawValue
        let header = LayoutController.header(ofPageNamed: "Joueurs", styledBy: path)
        
        return User.query(on: request).all().flatMap { users in
            let publicUsers = users.map { user in
                return user.toPublic()
            }
            
            let context = PlayersView(header, mySelf!.toPublic(), publicUsers)
            return try request.make(LeafRenderer.self).render(path, context)
        }
    }
    
    /// Launches the game.
    func play(_ request: Request) throws -> Future<Response> {
        let mySelf = try request.authenticated(User.self)
        let newRequest = request.redirect(to: "/").makeRequest()
        
        do {
            return try request.parameters.next(User.self).flatMap { user in
                return GameManager.isUserAvailableToPlay(whosIdendityIs: mySelf!, on: request).flatMap { isUserAvailableToPlay in
                    guard isUserAvailableToPlay else {
                        throw GameError.youAreAlreadyPlaying
                    }
                
                    return GameManager.isUserAvailableToPlay(whosIdendityIs: user, on: request).flatMap { isUserAvailableToPlay in
                        guard isUserAvailableToPlay else {
                            throw GameError.playerAlreadyPlaying
                        }
                
                        let game = Game(blackUserID: mySelf!.id!, whiteUserID: user.id!, isOpen: true)
                        
                        return game.create(on: request).flatMap { game in
                            return Board(forGame: game.id!).create(on: request).flatMap { board in
                                /*_ = BoardState(atTurn: .black, forBoard: board.id!).create(on: request).flatMap { boardState in
                                    return Score(forBoardState: boardState.id).create(on: request)
                                }*/
                                
                                return Score(forBoard: board.id).create(on: request).map { _ in
                                    return request.redirect(to: "/" + URLComponent.game.rawValue)
                                }
                            }
                        }
                    }
                }
            }
        } catch let error as ErrorHTTPBodyRepresentable {
            newRequest.http.body = error.convertToHTTPBody()
            return try request.redirect(to: "/" + URLComponent.players.rawValue).encode(for: newRequest)
        }
    }
    
    /// Allows to pass your turn.
    func pass(_ request: Request) throws -> Future<Response> {
        let mySelf = try request.authenticated(User.self)
        let newRequest = request.redirect(to: "/").makeRequest()
        
        
        return GameManager.getOpenGame(for: mySelf!, on: request).flatMap { game in
            /// Checks that a game is open for the user
            guard game != nil else {
                throw GameError.gameDoesNotExist
            }
                
            do {
                return try game!.board.query(on: request).first().flatMap { board in
                    guard board != nil else {
                        throw GameError.boardDoesNotExist
                    }
                    
                    /// Checks that it is the user's turn before allowing to pass
                    guard (board!.turn == .black && game!.blackUserID == mySelf!.id!) ||
                        (board!.turn == .white && game!.whiteUserID == mySelf!.id!) else {
                            throw GameError.notYourTurn
                    }
                    
                    return game!.blackUser.query(on: request).first().flatMap { blackUser in
                        return game!.whiteUser.query(on: request).first().flatMap { whiteUser in
                    
                            return try board!.score.query(on: request).first().flatMap { score in
                                return try board!.intersections.query(on: request).all().flatMap { intersections in
                                    let newHistoryEntry = BoardState(nil, forBoard: board!.id!, atTurn: board!.turn, on: Date(), score!, intersections)
                                    
                                    board!.joinedProperties.intersections = intersections
                                
                                    return try board!.history.query(on: request).sort(\BoardState.date, .ascending).all().flatMap { history in
                                        /// If game has an history and the opponent just passed his turn before the user did,
                                        /// then it must be stopped
                                        if history.count != 0 {
                                            return try history.last!.intersections.query(on: request).count().flatMap { intersectionsNumber in

                                                if intersectionsNumber == newHistoryEntry.joinedProperties.intersections!.count {
                                                    game!.isOpen = false
                                                    let newScore = board!.countScore()
                                                    
                                                    if newScore["black"]! > newScore["white"]! {
                                                        blackUser!.victories += 1
                                                        whiteUser!.defeats += 1
                                                    } else if newScore["black"]! > newScore["white"]! {
                                                        blackUser!.defeats += 1
                                                        whiteUser!.victories += 1
                                                    }
                                                    
                                                    return newHistoryEntry.save(on: request).flatMap { _ in
                                                        return game!.save(on: request).flatMap { _ in
                                                            return score!.save(on: request).flatMap { _ in
                                                                return blackUser!.save(on: request).flatMap { _ in
                                                                    return whiteUser!.save(on: request).map { _ in
                                                                        return request.redirect(to: "/")
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                                board!.flipTurn()
                                                return board!.save(on: request).flatMap { _ in
                                                    return newHistoryEntry.save(on: request).map { savedNewHistoryEntry in
                                                        _ = intersections.map { intersection in
                                                            intersection.id = nil
                                                            intersection.boardID = nil
                                                            intersection.boardStateID = savedNewHistoryEntry.id!
                                                            _ = intersection.create(on: request)
                                                        }
                                                        
                                                        return request.redirect(to: "/" + URLComponent.game.rawValue)
                                                    }
                                                }
                                            }
                                        }
                                        
                                        /// Otherwise, just pass the turn by saving history and the turn
                                        board!.flipTurn()
                                        return board!.save(on: request).flatMap { _ in
                                            return newHistoryEntry.save(on: request).map { _ in
                                                
                                                return request.redirect(to: "/" + URLComponent.game.rawValue)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch let error as ErrorHTTPBodyRepresentable {
                newRequest.http.body = error.convertToHTTPBody()
                return try request.redirect(to: "/" + URLComponent.game.rawValue).encode(for: newRequest)
            }
        }
    }
}
