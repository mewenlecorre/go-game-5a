import Vapor
import FluentMySQL
import Leaf
import Authentication

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    try services.register(FluentMySQLProvider())
    try services.register(LeafProvider())
    try services.register(AuthenticationProvider())
    
    services.register { container -> LeafTagConfig in
        var config = LeafTagConfig.default()
        config.use(EmptyTag(), as: "empty")
        return config
    }

    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)
    middlewares.use(SessionsMiddleware.self)
    
    services.register(middlewares)

    /// Configure a MySQL database
    let config = MySQLDatabaseConfig(hostname: "127.0.0.1", username: "root", password: "root", database: "GoGame")

    /// Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    databases.add(database: MySQLDatabase(config: config), as: .mysql)
    services.register(databases)

    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .mysql)
    migrations.add(model: Game.self, database: .mysql)
    migrations.add(model: Board.self, database: .mysql)
    migrations.add(model: BoardState.self, database: .mysql)
    migrations.add(model: Score.self, database: .mysql)
    migrations.add(model: Intersection.self, database: .mysql)
    migrations.add(model: DiscussionMessage.self, database: .mysql)


    //migrations.add(migration: AdminUser.self, database: .mysql)
    migrations.add(model: Token.self, database: .mysql)
    services.register(migrations)

    
    // Create a new NIO websocket server
    let webSockets = NIOWebSocketServer.default()
    
    // Add WebSocket upgrade support to GET /echo
    webSockets.get("chat", Game.parameter) { webSocket, request in
        //let mySelf = try request.requireAuthenticated(User.self)
        let _ = try request.parameters.next(Game.self).map { game -> () in
            // Add a new on text callback
            webSocket.onText { webSocket, text in
                do {
                    let json = try JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options : .allowFragments) as! [String: String]
                    let message = DiscussionMessage(forGame: UUID(uuidString: json["gameID"]!)!, sentBy: UUID(uuidString: json["authorID"]!)!, containing: json["content"]!, sentAt: Date())
                    let _ = message.save(on: request).map { savedMessage in
                        return savedMessage.author.get(on: request).map { author in
                            let hour = Calendar.current.component(.hour, from: savedMessage.date)
                            let minute = Calendar.current.component(.minute, from: savedMessage.date)
                            
                            let hourString = hour < 10 ? "0\(hour)" : "\(hour)"
                            let minuteString = minute < 10 ? "0\(minute)" : "\(minute)"
                            
                            let string: String = """
                            {"gameID":"\(savedMessage.gameID)","username":"\(author.username)","content":"\(savedMessage.content)","date":"\(hourString):\(minuteString)"}
                            """
                            webSocket.send(text: string)
                        }
                    }
                } catch let error {
                    print(error)
                }
            }
            
            let _ = DiscussionMessage.query(on: request).filter(\DiscussionMessage.gameID == game.id!).sort(\.date, .ascending).all().map { discussionMessages in
                return discussionMessages.compactMap { message in
                    return message.author.get(on: request).map { author -> String in
                        let hour = Calendar.current.component(.hour, from: message.date)
                        let minute = Calendar.current.component(.minute, from: message.date)
                        
                        let hourString = hour < 10 ? "0\(hour)" : "\(hour)"
                        let minuteString = minute < 10 ? "0\(minute)" : "\(minute)"
                        
                        let string: String = """
                        {"gameID":"\(message.gameID)","username":"\(author.username)","content":"\(message.content)","date":"\(hourString):\(minuteString)"}
                        """
                        
                        webSocket.send(text: string)
                        return string
                    }
                }
            }
        }
    }
    
    // Register our server
    services.register(webSockets, as: WebSocketServer.self)
}
