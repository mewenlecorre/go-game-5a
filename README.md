-- Go Game --

À la suite de ces quelques lignes, vous aurez lancé l'application "Go Game" en local sur votre machine.

L'application est réalisée en Swift avec le framework Vapor. Il est donc nécessaire d'avoir une machine tournant sous
MacOS ou Ubuntu pour pouvoir lancer le projet et d'effectuer les actions suivantes.

• Installer XCode (IDE conseillé) : https://developer.apple.com/xcode/

• Installer Swift : https://swift.org/getting-started/#installing-swift

• Installer Vapor sur MacOS : https://docs.vapor.codes/3.0/install/macos/

• Installer Vapor sur Ubuntu : https://docs.vapor.codes/3.0/install/ubuntu/

• Il vous faut également une base de données MySQL

Une fois ceci fait, il est nécessaire d'ouvrir le projet et de se rendre de le fichier /Sources/App/configure.swift et
repérer la ligne suivante :

> /// Configure a MySQL database

> let config = MySQLDatabaseConfig(hostname: "127.0.0.1", username: "root", password: "root", database: "GoGame")

Changez les quatre chaines de caractères en paramètre pour les faire correspondre aux identifiants permettant d'accéder
à votre base de données, Vapor s'occupera de créer les tables automatiquement.

Choisissez, à droite des boutons "build" et "stop", le schéma "Run" permettant de lancer l'application et puis cliquez sur
le bouton de build pour lancer l'application sur votre machine.